const gulp = require('gulp')
const autoprefixer = require('gulp-autoprefixer')
const cleanCSS = require('gulp-clean-css')
const browserSync = require('browser-sync')
const nodemon = require('gulp-nodemon')
const scss = require('gulp-sass')
const sourcemaps = require('gulp-sourcemaps')
const babel = require('gulp-babel')
const uglify = require('gulp-uglify')

gulp.task('default', ['browser-sync'], function () {
})

gulp.task('browser-sync', ['scss', 'js', 'nodemon'], function () {
  browserSync.init(null, {
    proxy: 'http://localhost:3000',
    files: ['public/**/*.*'],
    port: 7000
  })
  gulp.watch(['./src/scss/**/*.scss'], ['scss'])
  gulp.watch(['./src/js/**/*.js']).on('change', function (e) {
    return gulp.src(e.path)
      .pipe(sourcemaps.init())
      .pipe(babel({
        presets: ['es2015']
      }))
      .pipe(uglify())
      .pipe(sourcemaps.write('.'))
      .pipe(gulp.dest('./public/js'))
  })
  gulp.watch(['./views/**/*.pug']).on('change', browserSync.reload)
  gulp.watch(['./public/css/main.css']).on('change', function (e) {
    return gulp.src(e.path).pipe(browserSync.stream())
  })
})

gulp.task('scss', function () {
  gulp.src(['./src/scss/main.scss'])
    .pipe(sourcemaps.init())
    .pipe(scss().on('error', scss.logError))
    .pipe(autoprefixer())
    .pipe(cleanCSS())
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('./public/css'))
})

gulp.task('js', function () {
  gulp.src(['./src/**/*.js'])
    .pipe(sourcemaps.init())
    .pipe(babel({
      presets: ['es2015']
    }))
    .pipe(uglify())
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('./public'))
})

gulp.task('nodemon', function (cb) {
  var started = false
  return nodemon({
    script: './app',
    ignore: ['./public', './src']
  }).on('start', function () {
    if (!started) {
      cb()
      started = true
    }
  })
})

